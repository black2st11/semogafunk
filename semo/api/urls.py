from django.conf.urls import url, include
from api import views
from . import manager
from django.conf.urls.static import static
from django.conf import settings


manager_urlpatterns= [
    url(r'^manager/token/$', manager.CustomTokenView.as_view(), name='login'),
    url(r'^manager/user/$', manager.UserListView.as_view(), name='user'),
    url(r'^manager/value/$', manager.ValuationListView.as_view(), name='value'),
    url(r'^manager/verify/$', manager.VerifyListView.as_view(), name='value'),
    url(r'^manager/verify/(?P<valuation_id>\d+)/$', manager.VerifyView.as_view(), name='value')
]

urlpatterns = [
    url(r'^secret$', views.secret_page , name="secret"),
    url(r'^user/$', views.UserView.as_view()),
    url(r'^o/custom_token/$', views.Login.as_view()),
    url(r'^home/$', views.HomeView.as_view()),
    url(r'^valuations/(?P<valuation_id>\d+)/$', views.ValuationDetailView.as_view()),
    url(r'^valuations/$', views.ValuationView.as_view()),
    url(r'^valuate/(?P<valuation_id>\d+)/$', views.ValuationItemView.as_view()),
    url(r'^rank/$', views.ValuationRankingView.as_view()),
    url(r'^pass/$', views.ChangePasswordView.as_view()),
    url(r'^board/$', views.BoardView.as_view()),
    url(r'^board/(?P<board_id>\d+)/$', views.BoardDetailView.as_view()),
    url(r'myvaluations/$', views.MyValuationView.as_view()),
    url(r'^image/$', views.ImageView.as_view())

] +static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT) + manager_urlpatterns
