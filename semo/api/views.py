from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse
from rest_framework.views import APIView 
from rest_framework.permissions import AllowAny, IsAuthenticated
from api import serializers
import json
from core.models import Valuation, ValuationItem, ValuationImage, Board, ValuationTag, Tag
from django.db import transaction
from oauth2_provider.views import TokenView
from django.contrib.auth.models import User
from django.views.decorators.debug import sensitive_post_parameters
from django.utils.decorators import method_decorator
from django.forms.models import model_to_dict
from core.utils import agg, CustomPagination
import random
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import get_object_or_404
from operator import itemgetter
# Create your views here.

@login_required
def secret_page(request, *args, **kwargs):
    return HttpResponse('Secret contents', status=200)


#로그인
class Login(TokenView):
    @method_decorator(sensitive_post_parameters('password'))
    def post(self, request):
        print(request.POST)
        url, headers, body, status = self.create_token_response(request)
        return HttpResponse(body,status=status)

#유저
class UserView(APIView):
    # permission_classes = (AllowAny, IsAuthenticated)
    def post(self, request):
        data =request.data
        serializer = serializers.CreateUserSerailizer(data=data)
        if serializer.is_valid():
            try:
                with transaction.atomic():
                    user = serializer.save()
                    user.set_password(data['password'])
                    user.save()
                    return HttpResponse('good' ,status=200)
            except:
                pass
        
        return HttpResponse('good',status=200)

    def get(self,request):
        print(request.user)
        user = User.objects.get(username = request.user)
        serializer = serializers.UserSerializer(data=user)
        email = str(serializer.initial_data.email)
        username = str(serializer.initial_data)
        return HttpResponse(json.dumps({'email':email, 'username':username}, ensure_ascii=False),status=200)

    def put(self, request):
        user = User.objects.get(username= request.user)
        user.email = request.data['email']
        user.save()
        return HttpResponse('이메일 변경완료', status=200)

#비밀번호 변경
class ChangePasswordView(APIView):
    permission_classes = (IsAuthenticated,)
    def put(self, request):
        user = User.objects.get(username = request.user)
        user.set_password(request.data['password'])
        user.save()
        return HttpResponse('요청에 성공하셨습니다', status=200)
#홈
class HomeView(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        return HttpResponse('good', status=200)
#평가
class ValuationView(APIView):
    permission_classes= (IsAuthenticated,)
    #랜덤하게 평가 생성
    def get(self,request):

        # 본인이 평가한 항목 추출
        valuation = Valuation.objects.filter(owner=request.user) 
        serializer=serializers.ValuationListSerializer(valuation , many=True) 
        array = [o['valuationItem'] for o in serializer.data]

        # 본인이 평가한 항목의 제외하고 평가하지않은 항목만 반환
        valuations = ValuationItem.objects.all().exclude(id__in=array) 

        # 만약에 평가할 항목이 존재하지 않을 경우 0을 반환하여 모든 평가가 이루어졌음을 리스폰
        if len(valuations) ==0: 
            return HttpResponse(0, status=200)

        # 평가할 항목에 대하여 랜덤으로 뽑기 위해 Array의 크기에 따른 index 값  반환
        random_id = random.randint(0,len(valuations)-1) 

        # model 랜덤하게 설정
        serializer = serializers.ValuationItemListSerializer(valuations[random_id]) 
        value_image = ValuationImage.objects.get(owner=valuations[random_id])

        # 항목에 따른 이미지도 id와 동일하게 해서 반환 
        value_image_serial = serializers.ValuationImageSerializer(value_image) 

        # Json 파일로 반환
        return HttpResponse(json.dumps({'result':serializer.data,'image':value_image_serial.data['image']}, ensure_ascii=False), status=200) 
    def post(self, request):
        
        # 원자성 적용 (필요 데이터 중 없으면 모든 것을 commit 하지 않는다. 필요데이터 : name, price, image 선택사항 : tag)
        with transaction.atomic():
            # 평가 아이템 생성
            value = ValuationItem.objects.create(owner=request.user, name=request.data['name'], price=request.data['price'])
            # 평가 아이템을 외부 키로 설정하여 태그를 담을 DB 생성
            value_tag = ValuationTag.objects.create(valuation_item = value)

            # 태그 데이터가 존재할 때 저장 없을 시 그냥 넘어가게 해놓음 (약간 애매한 부분)
            if len(request.data['tag']) > 0:
                # 태그를 생성 및 조회하여 없을 시는 생성 있을 시에는 태그를 가져와서 삽입
                for i in request.data['tag']:
                    tag = Tag.objects.get_or_create(tag=i)
                    value_tag.tags.add(tag[0].id)
                    # commit 안해도 괜찮을 것 같은 느낌이 약간 있지만 나중에 하기로 하고 일단 save()
                    value_tag.save()


            # 태그 조회시 유용함
            # tag = Tag.objects.get(id=4)
            # print(tag.valuationtag_set.all())
            return HttpResponse(str(value.id), status=200)
        return HttpResponse('평가 생성에 실패했습니다.', status=400)


# JSON 파일로 Image를 읽지를 못해서 FormData 형식으로 받기위해 추가 ValuationView POST 와 연쇄적으로 일어난다.
class ImageView(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self,request):
        print(request.data)
        with transaction.atomic():
            value = ValuationItem.objects.get(id=int(request.data['id']))
            ValuationImage.objects.create(owner = value, image=request.data['image'])
            return HttpResponse('평가생성에 성공하였습니다.', status=200)
        return HttpResponse('평가생성에 실패하였습니다.', status=400)


#개별적 평가
class ValuationDetailView(APIView):
    permission_classes = (IsAuthenticated, )
    def get(self, request, valuation_id):
        value = ValuationItem.objects.get(id=valuation_id)
        serializer = serializers.ValuationItemDetailSerializer(value)
        return HttpResponse(json.dumps(serializer.data, ensure_ascii=False), status=200)

    def put(self, request, valuation_id):
        try:
            with transaction.atomic():
                value = ValuationItem.objects.filter(id= valuation_id)
                value_image = ValuationImage.objects.filter(owner__in=value)
                value.update(name=request.data['name'], price= request.data['price'])
                value_image.delete()
                ValuationImage.objects.create(owner = value[0], image = request.data['image'] )
                return HttpResponse('수정에 성공했습니다.', status=200)
        except:
            pass
        return HttpResponse('수정에 실패했습니다.', status=400)

    def delete(self, request, valuation_id):
        value = ValuationItem.objects.filter(id=valuation_id)
        value = value[0]
        if request.user == value.owner:
            value.delete()
        else :
            return HttpResponse('권한이 없습니다.', status=401)
            
        return HttpResponse('삭제에 성공했습니다.', status=200)


class ValuationItemView(APIView):
    permission_classes = (IsAuthenticated, )
    def get(self, request, valuation_id):
        get_object_or_404(Valuation, id=valuation_id)
        values = Valuation.objects.filter(valuationItem=valuation_id)
        return HttpResponse('good',status=200)

    def post(self,request, valuation_id):
        try:
            values = ValuationItem.objects.get(id=valuation_id)
            Valuation.objects.create(owner=request.user, valuationItem = values, price=request.data['price'])
        except ObjectDoesNotExist as e:
            return HttpResponse('평가한 항목이 존재하지 않습니다.', status =301)
        # Valuation.objects.create(owner=request.user , valuationItem = value[0], price=request.data['price'])
        return HttpResponse('평가가 등록되었습니다.', status =200)

    def delete(self, request, valuation_id):
        try:
            value = ValuationItem.objects.get(id=valuation_id)
            valuation = Valuation.objects.get(owner = request.user, valuationItem__in = value)
            valuation.delete()
        except ObjectDoesNotExist as e:
            return HttpResponse('요청하신 평가가 존재하지 않습니다.', status=301)
        return HttpResponse('평가가 삭제되었습니다.', status=200)

class ValuationRankingView(APIView):
    permission_classes = (IsAuthenticated, )
    def get(self, request):
        values = Valuation.objects.all()
        serializer = serializers.ValuationRankingSerializer(values ,many=True)
        result = agg(serializer.data)
        value_item = ValuationItem.objects.all()
        value_item_serializer = serializers.ValuationItemListSerializer(value_item, many=True)
        arr = []
        for i in value_item_serializer.data:
            for idx in result:
                if idx['id'] == str(i['id']): 
                    i['average_price'] = idx['price']
                    arr.append(i)
        data = sorted(arr, key = itemgetter('average_price'),reverse=True)
        return HttpResponse(json.dumps({'data':data}) ,status=200)

class BoardView(APIView):
    permission_classes = (IsAuthenticated , )
    def get(self, request):
        board = Board.objects.all()
        serializer = serializers.BoardListSerializer(board, many=True)
        return CustomPagination(serializer.data)

    def post(self, request):
        data =request.data
        Board.objects.create(owner=request.user, title=data['title'], content=data['content'])
        return HttpResponse('게시글 생성에 성공하셨습니다.', status=200)

class BoardDetailView(APIView):
    permission_classes = (IsAuthenticated, )
    def get(self,request, board_id):
        board = Board.objects.get(id=board_id)
        serializer = serializers.BoardDetailSerializer(board)
        return HttpResponse(json.dumps(serializer.data, ensure_ascii=False), status=200)
    def put(self, request, board_id):
        data = request.data
        board = Board.objects.get(id=board_id)
        if board.owner == request.user :
            board.title = data['title']
            board.content = data['content']
            board.save()
            return HttpResponse('게시글 수정을 완료했습니다.', status=200)
        else :
            return HttpResponse('본인의 게시글만 수정이 가능합니다.')
    
    def delete(self, request, board_id):
        board = Board.objects.get(id=board_id)
        if board.owner == request.user:
            board.delete()
            return HttpResponse('게시글 삭제를 완료했습니다..', status=200)        

        else:
            return HttpResponse('본인의 게시글만 삭제가능합니다.', status=200)        

class MyValuationView(APIView):
    permission_classes = (IsAuthenticated, )
    def get(self, request):
        values = Valuation.objects.all()
        serializer = serializers.ValuationRankingSerializer(values ,many=True)
        result = agg(serializer.data)
        value_item = ValuationItem.objects.filter(owner=request.user)
        value_item_serializer = serializers.ValuationItemListSerializer(value_item, many=True)
        arr = []
        for i in value_item_serializer.data:
            for idx in result:
                if idx['id'] == str(i['id']): 
                    i['average_price'] = idx['price']
                    arr.append(i)
        return HttpResponse(json.dumps({'data':arr,'origin_data':value_item_serializer.data}) ,status=200)

