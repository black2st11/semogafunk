import json
from datetime import datetime
import pytest
import pytest_django
from rest_framework import status
from rest_framework.reverse import reverse
from core import models
from django.contrib.auth.models import User

pytestmark = pytest.mark.django_db

class TestModel():
    def test_save(self):
        user = User.objects.create(username='black2st11', email='black2st11@gmail.com')
        user.set_password('linpose')
        user.save()
        value = models.ValuationItem.objects.create(owner=user, name='test', price=5000)
        assert value.name == 'test'
        assert value.price == 5000