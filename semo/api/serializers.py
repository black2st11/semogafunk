from rest_framework import serializers
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _
from core.models import ValuationItem, Valuation, ValuationImage, Board
from django.forms.models import model_to_dict
import json
class CreateUserSerailizer(serializers.ModelSerializer):
    password  = serializers.CharField(write_only=True)

    def validate(self, data):
        try:
            user = User.objects.filter(username=data.get('username'))
            if len(user)>0:
                raise serializers.ValidationError(_('이미 존재하는 아이디입니다.'))
        except :
            pass
        if not data.get('password'):
            raise serializers.ValidationError(_('비밀번호를 입력해주세요'))
        
        return data

    class Meta:
        model = User
        fields = ('id','email','username','password')

class ValuationItemListSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField()
    image = serializers.SerializerMethodField()
    
    class Meta:
        model = ValuationItem
        fields = ('id', 'owner','name','price', 'image', 'is_verify')

    def get_owner(self, value):
        return value.owner.username
    
    def get_image(self, value):
        values = ValuationImage.objects.get(owner = value)
        return str(values.image)

class ValuationRankingSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    class Meta:
        model = Valuation
        fields = '__all__'
    def get_name(self, value):
        return value.valuationItem.name

class ValuationItemDetailSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField()
    image = serializers.SerializerMethodField()
    
    class Meta:
        model = ValuationItem
        fields = ('id', 'owner','name','price','image')

    def get_owner(self, value):
        return value.owner.username

    def get_image(self, value):
        image_value = ValuationImage.objects.get(id=value.id)
        serializer = ValuationImageSerializer(image_value)
        value_image = str(image_value.image)
        return value_image

class ValuationImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ValuationImage
        fields = ('image',)

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class BoardListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Board
        fields = '__all__'

class BoardDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Board
        fields = '__all__'

class ValuationListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Valuation
        fields = '__all__'
