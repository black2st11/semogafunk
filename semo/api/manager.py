from rest_framework.views import APIView
from . import serializers
from core import models
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAdminUser
from oauth2_provider.views import TokenView
from django.views.decorators.debug import sensitive_post_parameters
from django.utils.decorators import method_decorator
from django.http.response import HttpResponse
from oauth2_provider.models import get_access_token_model
from oauth2_provider.signals import app_authorized
from oauth2_provider.models import AccessToken
from django.core.exceptions import ObjectDoesNotExist

from core import models
import json


# token정보에 최고관리자유무 포함하기 위해 customview 구현
class CustomTokenView(TokenView):
    @method_decorator(sensitive_post_parameters("password"))
    def post(self, request, *args, **kwargs):
        url, headers, body, status = self.create_token_response(request)
        if status == 200:
            body = json.loads(body)
            access_token = body.get("access_token")
            if access_token is not None:
                token = get_access_token_model().objects.get(
                    token=access_token)
                app_authorized.send(
                    sender=self, request=request,
                    token=token)
                body['is_admin'] = token.user.is_superuser
                body = json.dumps(body)
                # 중복로그인 제외하기 위해서 로그인 발생시 이전 토큰 삭제
                AccessToken.objects.filter(application_id=token.application_id, user=token.user).exclude(token=token.token).delete()
        response = HttpResponse(content=body, status=status)
        for k, v in headers.items():
            response[k] = v


        return response

class UserListView(APIView):
    permission_classes = (IsAdminUser,)
    def get(self, request):
        user = models.User.objects.all()
        serializer =  serializers.UserSerializer(user, many=True)
        return HttpResponse(json.dumps(serializer.data, ensure_ascii=False) , status=200)

class ValuationListView(APIView):
    permission_classes = (IsAdminUser,)
    def get(self, request):
        value = models.ValuationItem.objects.all()
        serializer = serializers.ValuationItemListSerializer(value, many=True)
        return HttpResponse(json.dumps(serializer.data, ensure_ascii=False),status=200)

class VerifyListView(APIView):
    permission_classes = (IsAdminUser,)
    def get(self, request):
        value = models.ValuationItem.objects.filter(is_verify=False)
        serializer = serializers.ValuationItemListSerializer(value, many=True)
        return HttpResponse(json.dumps(serializer.data, ensure_ascii=False),status=200)

class VerifyView(APIView):
    permission_classes = (IsAdminUser, )
    def put(self, request, valuation_id):
        try:
            value = models.ValuationItem.objects.get(id=valuation_id)
        except ObjectDoesNotExist as e:
            return HttpResponse('해당평가가 존재하지않습니다.', stats=301)
        value.is_verify = True
        value.save()
        return HttpResponse('good', status=200)

    def delete(self, request, valuation_id):
        try :
            value = models.ValuationItem.objects.get(id=valuation_id)
        except ObjectDoesNotExist as e:
            return HttpResponse('해당평가가 존재하지않습니다.' ,status=301)
        value.delete()
        return HttpResponse('good', status=200)