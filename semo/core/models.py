from django.db import models
from django.contrib.auth.models import User
import uuid
# Create your models here.

def valuation_image(instance, filename):
    return "valuation/image/{}/{}.jpeg".format(instance.id,uuid.uuid4())


class ValuationItem(models.Model):
    owner = models.ForeignKey(User,on_delete=models.CASCADE ,related_name='items', related_query_name='item')
    name = models.CharField(max_length=30)
    price = models.IntegerField()
    is_verify = models.BooleanField(default=False)
    created_time = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified_time = models.DateTimeField(auto_now=True, null=True, blank=True)

class Valuation(models.Model):
    owner = models.ForeignKey(User,on_delete=models.CASCADE ,related_name='valuations', related_query_name='valuation')
    valuationItem = models.ForeignKey(ValuationItem,on_delete=models.CASCADE ,related_name='valuationitems', related_query_name='valuationitem')
    price = models.IntegerField()
    created_time = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified_time = models.DateTimeField(auto_now=True, null=True, blank=True)

class ValuationImage(models.Model):
    owner = models.ForeignKey(ValuationItem, on_delete=models.CASCADE, related_name='valuation_images', related_query_name='valuation_image')
    image = models.ImageField(upload_to=valuation_image)
    created_time = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified_time = models.DateTimeField(auto_now=True, null=True, blank=True)

class Board(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='boards', related_query_name='board')
    title = models.CharField(max_length=30)
    content = models.TextField()
    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)

class Tag(models.Model):
    tag = models.CharField(max_length=20)
    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)

class ValuationTag(models.Model):
    valuation_item = models.ForeignKey(ValuationItem, on_delete=models.CASCADE,related_name='valutaion_tags', related_query_name='valuation_tag')
    tags = models.ManyToManyField(Tag)
    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)