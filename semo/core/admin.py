from django.contrib import admin
from .models import ValuationItem, Valuation, ValuationImage, Tag, ValuationTag
# Register your models here.

class ValuationAdmin(admin.ModelAdmin):
    list_display=['owner','valuationItem']

admin.site.register(Valuation, ValuationAdmin)
admin.site.register(ValuationItem)
admin.site.register(ValuationImage)
admin.site.register(ValuationTag)
admin.site.register(Tag)