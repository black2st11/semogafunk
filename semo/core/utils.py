from rest_framework import pagination
from rest_framework.response import Response
def agg(data):
    sumdict = {}
    countdict = {}
    arrList = []
    result = []
    # 전체 평가되어진 항목을 인자로 받아서 id 별로 arrList에 삽입
    for i in range(len(data)):
        if data[i]['valuationItem'] not in arrList:
            arrList.append(data[i]['valuationItem'])
            # 항목이 이미 arrList에 삽입되어지 경우(데이터가 있다면 최소 한번은 실행되어짐)
        if data[i]['valuationItem'] in arrList:
            # 기존에 있던 값일 경우 data를 더하고 카운트도 더해준다.
            try:
                sumdict[str(data[i]['valuationItem'])] += data[i]['price']
                countdict[str(data[i]['valuationItem'])] += 1
            except :
            #만약에 처음 들어온 값일 경우 해당 변수의 값을 대입시켜준다.
                sumdict[str(data[i]['valuationItem'])] = data[i]['price']
                countdict[str(data[i]['valuationItem'])] = 1
    # key값에 따른 count로 나누어주어서 평균값 산출
    for key in countdict.keys():
        result.append( {'id':key,'price':sumdict[key]/countdict[key]} ) 
    return (result)


class CustomPagination(pagination.PageNumberPagination):
    def get_paginated_response(self, data):
        return Response({
            'links':{
                'next': self.get_next_link(),
                'previous' : self.get_previous_link()
            },
            'count': self.page.paginator.count,
            'result': data
        })
